#include <avr/io.h>
#include "lcd.h"

// used for printf operation
static int lcd_putchar(char c);//, FILE *stream);
static FILE mystdout = FDEV_SETUP_STREAM(lcd_putchar, NULL,
                                             _FDEV_SETUP_WRITE);
// delay
void delay_us1(uint8_t us);
int satuan;
int puluhan;
int ratusan;


void clock_e (void)
{
	LCD_E_PORT |= (1<<E);
	delay_us1(5);
	LCD_E_PORT &= ~(1<<E);
	delay_us1(3);
}

void write_command(uint8_t command)
{
	// clear RS pin
	LCD_RS_PORT &= ~(1<<RS);
#ifdef HIGH_NIBBLE
	LCD_PORT = (LCD_PORT&0x0f)|(command&0xf0);
	clock_e();
	LCD_PORT = (LCD_PORT&0x0f)|((command&0x0f)<<4);
	clock_e();
#else
	LCD_PORT = (LCD_PORT&0xf0)|((command&0xf0)>>4);
	clock_e();
	LCD_PORT = (LCD_PORT&0xf0)|(command&0x0f);
	clock_e();	
#endif
	uint8_t i;
	for(i=0;i<20;i++)
		delay_us1(100);
}

void lcd_init (void)
{
	uint8_t i;
// delay 15 ms for LCD to power up
	for(i=0;i<150;i++)
		delay_us1(100);
// set all data and control port as output
#ifdef HIGH_NIBBLE
	LCD_DDR |= 0xf0;
#else
	LCD_DDR |= 0x0f;
#endif
	LCD_RS_DDR |= (1<<RS);
	LCD_E_DDR |= (1<<E);
	// clear RS pin
	LCD_RS_PORT &= ~(1<<RS);

// 4 bit mode write squence
#ifdef HIGH_NIBBLE
	LCD_PORT = (LCD_PORT&0x0f)|0x30;
#else
	LCD_PORT = (LCD_PORT&0xf0)|0x03;
#endif
	clock_e();
	// 4.1 ms delay
	for(i=0;i<41;i++)
		delay_us1(100);
	clock_e();
	// 100 delay
	delay_us1(100);
	clock_e();
	// 40 us delay
	delay_us1(40);
	// 4 bit mode setting
	// 4 bit 2 line 5 X 7 dot matrix
	write_command(0x28);
	// increment cursor after write byte
	write_command(0x06);
	// display on cursor off
	write_command(0x0C);
	clrscr ();
	// set function for printf
	stdout = &mystdout;
}

void goto_xy(uint8_t row,uint8_t column)
{
	if (row == 1)
		write_command (0x80 + column - 1);
	else
		write_command (0xC0 + column - 1);
}

void clrscr (void)
{
	write_command(0x01);
}

static int lcd_putchar(char c)//, FILE *stream)
{
	// set RS pin
	LCD_RS_PORT |= (1<<RS);
#ifdef HIGH_NIBBLE
	LCD_PORT = (LCD_PORT&0x0f)|(c&0xf0);
	clock_e();
	LCD_PORT = (LCD_PORT&0x0f)|((c&0x0f)<<4);
	clock_e();
#else
	LCD_PORT = (LCD_PORT&0xf0)|((c&0xf0)>>4);
	clock_e();
	LCD_PORT = (LCD_PORT&0xf0)|(c&0x0f);
	clock_e();	
#endif
	// 2 ms delay
	uint8_t i;
	for(i=0;i<20;i++)
		delay_us1(100);
	return 0;
}

void delay_us1(uint8_t us)
{
	TCNT0 = 257-us;
	TCCR0 |= (1<<CS01);
	loop_until_bit_is_set(TIFR,TOV0);
	TIFR |= (1<<TOV0);
	TCCR0 &= ~(1<<CS01);
}


void LCD_putstr (char *s)		
{
	while (*s)
		lcd_putchar (*s++);
}

void LCD(char c)		
{
						// Tunggu hingga LCD siap menerima perintah baru
	lcd_putchar(c);			// Tulis data register dengan memberikan H-to-L transisi pada pin E
}


void as(int VALUE, char digit);
//as(get_distance(LEFT),3);  //3=jumlah digit
void as(int VALUE, char digit)
{

		satuan = (VALUE%10)+0x30;
		puluhan = (VALUE%100)/10;
		puluhan += 0x30;		 
		ratusan = (VALUE % 1000)/100;
		ratusan +=0x30;
		char ribuan = (VALUE%10000)/1000;
		ribuan +=0x30;	
		char puluhanribu = (VALUE%100000)/10000;
		puluhanribu +=0x30;
			 
		if(digit == 1) 
		{			 
			 LCD(satuan);	
		}
		else if(digit == 2)
		{			
			 LCD(puluhan);
			 LCD(satuan);
		
		}	 
		else if(digit == 3)
		{	
			 LCD(ratusan);
			 LCD(puluhan);
			 LCD(satuan);
		}
		else if(digit == 4)
		{
			LCD(ribuan);
			 LCD(ratusan);
			 LCD(puluhan);
			 LCD(satuan);
		}	 

		else if(digit == 5)
		{
			 LCD(puluhanribu);
			 LCD(ribuan);
			 LCD(ratusan);
			 LCD(puluhan);
			 LCD(satuan);
		}
			
	
}
