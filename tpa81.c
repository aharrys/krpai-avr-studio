#include <avr/io.h>
#include "semua.h"


unsigned char sudut (unsigned char y);
unsigned char panas (unsigned char t);
void i2c_write (uint8_t data1);
uint8_t i2c_read (uint8_t data2);
void i2c_stop (void);
unsigned char suhu;

void i2c_start (void);
//========================================
//========================================

unsigned char panas (unsigned char t)
{	
	i2c_start (); 			//start kondisi
	i2c_write (0xD0);		//kirim data SLA+W ke kompas
	i2c_write (t);			//minta data dari kompas pada register 0
	i2c_start ();			//repeat start
	i2c_write (0XD1);			//minta data dari kompas pada register 1
	suhu = i2c_read(255);
	i2c_stop();
	return suhu;

}

unsigned char sudut (unsigned char y)
{	
	i2c_start (); 			//start kondisi
	i2c_write (0xC0);		//kirim data SLA+W ke kompas
	i2c_write (y);			//minta data dari kompas pada register 0
	i2c_start ();			//repeat start
	i2c_write (0XC1);			//minta data dari kompas pada register 1
	suhu = i2c_read(255);
	i2c_stop();
	return suhu;

}

//========================================
//========================================

void servo(uint8_t posisi)
{
	i2c_start (); 
	i2c_write (0xD0);
	i2c_write (0x00);
	i2c_write (posisi);
	i2c_stop();

}
//========================================
//========================================
void i2c_start (void)
{
	PORTC |= 1<<PC1;
	PORTC |=1<<PC0;

	TWCR=(1<<TWINT)|(1<<TWSTA)|(1<<TWEN); 	//TWINT TWISTA(start) TWEN (enable) diset
	loop_until_bit_is_set(TWCR,TWINT);		// cek TWINT apakah sudah set
}

//========================================
//========================================
void i2c_write (uint8_t data1)
{	
	TWDR = data1; 							//SLA+W ditulis ke TWDR, nilai SLA+W adalah 0xC0
	TWCR = (1<<TWINT)|(1<<TWEN)	; 
	loop_until_bit_is_set(TWCR,TWINT); 		// cek TWINT apakah sudah set
}
//========================================
//========================================
uint8_t i2c_read (uint8_t data2)
{	
	TWDR = data2; 							//SLA+R ditulis ke TWDR, nilai SLA+R adalah 0xC1
	TWCR = (1<<TWINT) | (1<<TWEN);
	loop_until_bit_is_set(TWCR,TWINT); 		// cek TWINT apakah sudah set

	return TWDR;							//derajat terukur
}
//========================================
//========================================
void i2c_stop (void)
{
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO); 	//stop operasi
	PORTC |= 0<<PC1;
	PORTC |=0<<PC0;

}

//========================================
//========================================
