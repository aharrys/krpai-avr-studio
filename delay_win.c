/* Fungsi untuk membuat delay dalam us dan ms */

#include <avr/io.h>
#include "delay_win.h"
#include "semua.h"


void delay_ms (int ms)	// fungsi loncat delay ms
{
	for (i=0;i<ms;i++)
	{
	_delay_ms(1);
	}

}

//------------------------------------------------------------------------------
// delay yang bisa berubah-ubah ditambah dengan 0.37us
//------------------------------------------------------------------------------
void delay_us(uint8_t s)
{
	TCCR0=_BV(CS01);
	TCNT0=(257-s);
	loop_until_bit_is_set(TIFR,TOV0);
	TIFR|=_BV(1<<TOV0);	
}

//------------------------------------------------------------------------------
// delay untuk triger uson
//------------------------------------------------------------------------------

void delay_025ms(uint8_t s)				//delay untuk hitung uson per 1 cm
{
	TCNT0 = 255-s; 
	TCCR0 =_BV(CS02)|_BV(CS00);			// Start Timer 0 dengan prescaler 1024
	loop_until_bit_is_set(TIFR,TOV0);
	TCCR0 = 0;					// Stop timer
	TIFR = _BV(OCF0)|_BV(TOV0);
}

void lama_detik(uint8_t i)
{
	uint8_t t;
	for (t=0; t<=i; t++ )
	{
		delay_025ms(255);
		delay_025ms(255);
		delay_025ms(255);
		delay_025ms(255);
	}
}

//delay per 1 detik (delay dia ambil dari delay.h punya avr)

/*void _delay_1detik (uint8_t s)
{
	uint16_t i;
	uint8_t j;

	for(j=0;j<s;j++)
	{
		for(i=0;i<40000;i++)
		_delay_ms(25);
	{
}*/
