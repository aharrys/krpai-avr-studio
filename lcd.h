#include <util/delay.h>
#include <stdio.h>
#define LCD_PORT	PORTB		// PORT yang digunakan untuk data
#define LCD_DDR		DDRB

								// bit used (low(D3-D0) or high nibble(D7-D4)
#define HIGH_NIBBLE				// uncomment this if low nibble pin used

#define LCD_RS_PORT	PORTB		// control RS PORT pin

#define LCD_RS_DDR	DDRB		// control RS DDR pin

#define RS			0			// RS Pin


#define LCD_E_PORT	PORTB		// control E PORT pin

#define LCD_E_DDR	DDRB		// control E DDR pin

#define E			1			// E Pin

void LCD_putstr (char *s)	;

void lcd_init (void);			// inisialisasi LCD

void goto_xy (uint8_t row,uint8_t column);	// koordinat tempat karakter

void clrscr (void);				// membersihkan display

void LCD_putcharX(char c);
