#include <avr/io.h>
#include <util/delay.h>
#include "semua.h"

uint16_t DCL,DCR;							// deklarasi fungsi DCL, DCR
uint16_t error;
int prop,integ,diff,ts, lastError,rateInt, Ts, rate,mv, R;
int kp,kd,ki,ref_jarak,kec_max,kec_min, inte,inte1;

/////////////////TELUSUR KIRI/////////////////////////////////////////////////////////

	void TL()
	// BISMILLLAH !	
			{
		if bit_is_clear(PIND,PIND1)
		{
		clrscr();
				goto_xy(1,1);printf("switch aktif");
				
		mundur();pwm(200,200);_delay_ms(100);
		belok_kanandikit();pwm(150,150);_delay_ms(300);
		maju();pwm(100,100);_delay_ms(200);
		stop();_delay_ms(10);

		}

		if ( IR_depankanan() < 210 && IR_depankiri() <210 && get_distance(Front) > 40 ) // 
		{				
				
			
					
					kp=13; // 13
					kd=23;
					ki=0.00001;
					ref_jarak=23; //25
					kec_max=200; //130
					kec_min=0;
					
					R= get_distance(FL)  ;
					error = ref_jarak - R;
					rate = error - lastError; //derivative program 
					rateInt = error + lastError;
					lastError=error; 
					Ts=1;
					prop = kp*error;
					diff = (kd/Ts) * rate;
					inte = Ts * (ki * rateInt);
					inte = inte1 + error;
    				inte1 = (inte * ki); 
					
					mv = prop + diff + inte1 ;
					
					 DCL= kec_max  + mv;
			         DCR= kec_max - mv;

		 		
					
				if(mv > 0 && mv < kec_max || mv <0 && mv > - (kec_max) )
				{
					if (DCL > kec_max )
						{
						DCL= kec_max;
						}
		
					else if (DCL < kec_min &&  mv <0 && mv > - (kec_max))
						{
						DCL= kec_min;
						}
			
					if ( DCR > kec_max && mv <0 && mv > - (kec_max) )
						{
						DCR= kec_max;
						}
	
					else if ( DCR < kec_min &&  mv <0 && mv > - (kec_max) )
						{
						DCR= kec_min;
						}
		

				maju();
				pwm(DCR,DCL);



				}

				else if (mv > kec_max )
				{

			
						maju();	
					//	belok_kanandikit();
						pwm(70,100);// R===L 50 130
						
				}
				else if (mv < - (kec_max))
				{
				 	
				
			
							belok_kiridikit();
							pwm(220,160);// L==R 180 = 100
					
				}


		}

		
	

		else
		{
		belok_kanandikit();
		pwm(190,240);
		
		}

				clrscr();
					goto_xy(1,1);
					printf("mv=%3d",mv);
					goto_xy(1,9);
					printf("k=%3d",kondisiZ);
					goto_xy(2,1);
					printf("r=%3d",DCR);
					goto_xy(2,7);
					printf("l=%3d",DCL);

			
			}

/////////////////////////////////////////////////////////////////////////
	void TL_api()
			{

			if bit_is_clear(PIND,PIND1)
		{
		clrscr();
				goto_xy(1,1);printf("switch aktif");
				
		mundur();pwm(200,200);_delay_ms(100);
		belok_kanandikit();pwm(150,150);_delay_ms(300);
		maju();pwm(100,100);_delay_ms(200);
		stop();_delay_ms(10);

		}
		else if (line_depan()>140 && line_depan1()>100  )
				{
				clrscr();
				goto_xy(1,1);
				printf("MATIIN API !!");
				stop();_delay_ms(10);
				brake();
				brake();
				stop();_delay_ms(1000);
				while(line_depan()<160 || line_depan1()<120)
					{
					maju();
					pwm(50,50);
				
					}
				cari_api();
				kondisiZ=3;
					
				
				}

// BISMILLLAH !	
		else if (IR_depankanan() < 240 && IR_depankiri() <240 && get_distance(Front) >20 ) // 
		{				
				
			
					
					kp=13; // 13
					kd=20;
					ki=0.00001;
					ref_jarak=25; //25
					kec_max=140; //30
					kec_min=0;
///////////++++++++++++++++++++++++++++++++++//////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////						
					R= get_distance(FL) ;
					error = ref_jarak - R;
					rate = error - lastError; //derivative program 
					rateInt = error + lastError;
					lastError=error; 
					Ts=1;
					prop = kp*error;
					diff = (kd/Ts) * rate;
					inte = Ts * (ki * rateInt);
					inte = inte1 + error;
    				inte1 = (inte * ki); 
					
					mv = prop + diff + inte1 ;
					
					DCL= kec_max  + mv;
			        DCR= kec_max - mv;

		 		
					
				if(mv > 0 && mv < kec_max || mv <0 && mv > - (kec_max) )
				{
					if (DCL > kec_max )
						{
						DCL= kec_max;
						}
		
					else if (DCL < kec_min &&  mv <0 && mv > - (kec_max))
						{
						DCL= kec_min;
						}
			
					if ( DCR > kec_max && mv <0 && mv > - (kec_max) )
						{
						DCR= kec_max;
						}
	
					else if ( DCR < kec_min &&  mv <0 && mv > - (kec_max) )
						{
						DCR= kec_min;
						}
		

				maju();
				pwm(DCR,DCL);



				}

				else if (mv > kec_max)
				{

			
					
						maju();
						pwm(60,100);// R===L 50 130
						
				}
				else if (mv < - (kec_max))
				{
				 	
				
			
							belok_kiridikit();
							pwm(230,160);// L==R 
					
				}


		}

		else
		{
		belok_kanandikit();
		pwm(190,240);
		
		}

				clrscr();
					goto_xy(1,1);
					printf("mv=%3d",mv);
					goto_xy(1,9);
					printf("k=%3d",kondisiZ);
					goto_xy(2,1);
					printf("r=%3d",DCR);
					goto_xy(2,7);
					printf("l=%3d",DCL);

					

			
			}

////////////////////////TELUSUR KANAN//////////////////////////////////
void TR_api()
			{

		if bit_is_clear(PIND,PIND1)
		{
		clrscr();
				goto_xy(1,1);printf("switch aktif");
				
		mundur();pwm(200,200);_delay_ms(100);
		belok_kiridikit();pwm(150,150);_delay_ms(300);
		maju();pwm(100,100);_delay_ms(200);
		stop();_delay_ms(10);

		}
		else if (line_depan()>140 && line_depan1()>100  )
				{
				clrscr();
				goto_xy(1,1);
				printf("MATIIN API !!");
				stop();_delay_ms(10);
				brake();
				brake();
				stop();_delay_ms(1000);
				while(line_depan()<160 || line_depan1()<120)
					{
					maju();
					pwm(50,50);
				
					}
				cari_api();
				kondisiZ=4;
				
				
				}

// BISMILLLAH !	
		else if ( IR_depankanan() < 240 && IR_depankiri() <240 && get_distance(Front) > 20  ) // 40 (masukin IR sekalian)
		{				
				
			
					
					kp=13; // 13
					kd=25;
					ki=0.00001;
					ref_jarak=28; //25
					kec_max=140; //130
					kec_min=0;
///////////++++++++++++++++++++++++++++++++++//////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////						
					R= get_distance(FR);
					error = ref_jarak - R;
					prop = kp*error;
					rate = error - lastError; //derivative program 
					rateInt = error + lastError;
					lastError=error; 
					Ts=1;
					diff = (kd/Ts) * rate;
					inte = Ts * (ki * rateInt);
					inte = inte1 + error;
    				inte1 = (inte * ki);
					
					mv = prop+diff+inte1  ;
					
					 DCL= kec_max  + mv;
			         DCR= kec_max - mv;

		 		
					
				if(mv > 0 && mv < kec_max || mv <0 && mv > - (kec_max) )
				{
					if (DCL > kec_max )
						{
						DCL= kec_max;
						}
		
					else if (DCL < kec_min &&  mv <0 && mv > - (kec_max))
						{
						DCL= kec_min;
						}
			
					if ( DCR > kec_max && mv <0 && mv > - (kec_max) )
						{
						DCR= kec_max;
						}
	
					else if ( DCR < kec_min &&  mv <0 && mv > - (kec_max) )
						{
						DCR= kec_min;
						}
		

				maju();
				pwm(DCL,DCR);



				}

				else if (mv < - (kec_max))
				{

							
						
						belok_kanandikit();// disini belum fix antara dibuat maju apa patah
						pwm(120,230);// R===L 50 130
						
				}
				else if (mv > (kec_max))
				{
				maju();
				pwm(100,60);// L==R 

				}

		

		}
		
		
		else
		{
		belok_kiridikit();
		pwm(240,190);
		
		}

			clrscr();
					goto_xy(1,1);
					printf("mv=%3d",mv);
					goto_xy(1,9);
					printf("k=%3d",kondisiZ);
					goto_xy(2,1);
					printf("r=%3d",DCR);
					goto_xy(2,7);
					printf("l=%3d",DCL);

								


			}


void TR()
			{

		if bit_is_clear(PIND,PIND1)
		{
		clrscr();
				goto_xy(1,1);printf("switch aktif");
				
		mundur();pwm(200,200);_delay_ms(100);
		belok_kiridikit();pwm(100,100);_delay_ms(300);
		maju();pwm(100,100);_delay_ms(200);
		stop();_delay_ms(10);

		}

// BISMILLLAH !	
		else if ( IR_depankanan() < 210 && IR_depankiri() <210 && get_distance(Front) > 30 ) // 40 (masukin IR sekalian)
		{				
				
			
					
					kp=13; // 13
					kd=25;
					ki=0.00001;
					ref_jarak=28; //25
					kec_max=200; //130
					kec_min=0;
///////////++++++++++++++++++++++++++++++++++//////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////						
					R= get_distance(FR);
					error = ref_jarak - R;
					prop = kp*error;
					rate = error - lastError; //derivative program 
					rateInt = error + lastError;
					lastError=error; 
					Ts=1;
					diff = (kd/Ts) * rate;
					inte = Ts * (ki * rateInt);
					inte = inte1 + error;
    				inte1 = (inte * ki);
					
					mv = prop+diff+inte1  ;
					
					 DCL= kec_max  + mv;
			         DCR= kec_max - mv;

		 		
					
				if(mv > 0 && mv < kec_max || mv <0 && mv > - (kec_max) )
				{
					if (DCL > kec_max )
						{
						DCL= kec_max;
						}
		
					else if (DCL < kec_min &&  mv <0 && mv > - (kec_max))
						{
						DCL= kec_min;
						}
			
					if ( DCR > kec_max && mv <0 && mv > - (kec_max) )
						{
						DCR= kec_max;
						}
	
					else if ( DCR < kec_min &&  mv <0 && mv > - (kec_max) )
						{
						DCR= kec_min;
						}
		

				maju();
				pwm(DCL,DCR);



				}

				else if (mv < - (kec_max))
				{

							
						
						belok_kanandikit();// disini belum fix antara dibuat maju apa patah
						pwm(160,230);// R===L 50 130
						
				}
				else if (mv > (kec_max)  )
				{
				maju();
				pwm(80,50);// L==R 

				}

		

		}
		
		
		else
		{
		belok_kiridikit();
		pwm(240,190);
		
		}

			clrscr();
					goto_xy(1,1);
					printf("mv=%3d",mv);
					goto_xy(1,9);
					printf("k=%3d",kondisiZ);
					goto_xy(2,1);
					printf("r=%3d",DCR);
					goto_xy(2,7);
					printf("l=%3d",DCL);
				


			}
