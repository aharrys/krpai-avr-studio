#include <avr/io.h>
#include <util/delay.h>
#include "semua.h"

#define    DDR_motor    DDRD    //DDR untuk Motor
#define    PORT_motor    PORTD    //PORT untuk Motor

#define PWMKANAN         5        // Enable L298 untuk motor kanan
#define dirA_ka     7        // Direction A untuk motor kanan
#define dirB_ka     6        // Direction B untuk motor kanan
#define pwm_kanan    OCR1A

#define PWMKIRI        4       // Enable L298 untuk motor kanan
#define dirA_ki     2        // Direction A untuk motor kiri
#define dirB_ki     3        // Direction B untuk motor kiri
#define pwm_kiri    OCR1B


uint16_t DCL,DCR;							// deklarasi fungsi DCL, DCR
uint16_t error;
int prop,integ,diff,ts, lastError,rateInt, Ts, rate,mv, R;
int kp,kd,ki,ref_jarak,kec_max,kec_min, inte;


void pwm(uint8_t dcL,uint8_t dcR)
{	
	OCR1A = dcL;
	OCR1B = dcR;
//	dcL=PWMKIRI;
//	dcR=PWMKANAN;
	
}




void tes_motor()
{
	maju();
	pwm(2,200);_delay_ms(2000);
	maju();
	pwm(20,150);_delay_ms(2000);
	maju();
	pwm(50,100);_delay_ms(2000);
	maju();
	pwm(100,50);_delay_ms(2000);
	maju();
	pwm(150,20);_delay_ms(2000);
	maju();
	pwm(200,2);_delay_ms(2000);
	stop();_delay_ms(3000);

}



		
/////////////////////////////////////////////////////////////////////////////////////




void maju ()			
{


	PORT_motor |=(1<<dirB_ka);        //motor mundur
	PORT_motor &=~(1<<dirA_ka);

	PORT_motor |=(1<<dirB_ki);        //motor mundur
	PORT_motor &=~(1<<dirA_ki);
}

void mundur()
{

	PORT_motor |=(1<<dirA_ka);        //motor maju
	PORT_motor &=~(1<<dirB_ka);

	PORT_motor |=(1<<dirA_ki);        //motor maju
	PORT_motor &=~(1<<dirB_ki);
	pwm(100,100);

}

void brake()
{

	PORT_motor |=(1<<dirA_ka);        //motor maju
	PORT_motor &=~(1<<dirB_ka);

	PORT_motor |=(1<<dirA_ki);        //motor maju
	PORT_motor &=~(1<<dirB_ki);
	pwm(120,120);
	_delay_ms(100);

}

void stop ()			// fungsi loncat stop
{

	PORT_motor &=~(1<<dirA_ka);        //motor maju
	PORT_motor &=~(1<<dirB_ka);

	PORT_motor &=~(1<<dirA_ki);        //motor maju
	PORT_motor &=~(1<<dirB_ki);
	pwm(0,0);

	_delay_ms(200);
}



void belok_kiri()
{	


	PORT_motor &=~(1<<dirA_ka);        //motor maju
	PORT_motor |=(1<<dirB_ka);

	PORT_motor |=(1<<dirA_ki);        //motor maju
	PORT_motor &=~(1<<dirB_ki);
	pwm(230,230);

   
}

void muter_kiri()
{
	PORT_motor |=(1<<dirB_ka);        //motor mundur
	PORT_motor &=~(1<<dirA_ka);
	PORT_motor &=~(1<<dirA_ki);        //motor maju
	PORT_motor &=~(1<<dirB_ki);

		pwm(230,230);

}

void belok_kiridikit()
{	

	PORT_motor &=~(1<<dirA_ka);        //motor maju
	PORT_motor |=(1<<dirB_ka);

	PORT_motor |=(1<<dirA_ki);        //motor maju
	PORT_motor &=~(1<<dirB_ki);
		pwm(230,230);

   
}

void belok_kanan()
{
	PORT_motor |=(1<<dirA_ka);        //motor maju
	PORT_motor &=~(1<<dirB_ka);

	PORT_motor &=~(1<<dirA_ki);        //motor maju
	PORT_motor |= (1<<dirB_ki);
	pwm(230,230);
}

void muter_kanan()
{
	PORT_motor |=(1<<dirB_ki);        //motor mundur
	PORT_motor &=~(1<<dirA_ki);
	PORT_motor &=~(1<<dirA_ka);        //motor maju
	PORT_motor &=~(1<<dirB_ka);


		pwm(230,230);
//	pwm(220,220);
//	_delay_ms(5000);

}


void belok_kanandikit()
{
	PORT_motor |=(1<<dirA_ka);        //motor maju
	PORT_motor &=~(1<<dirB_ka);

	PORT_motor &=~(1<<dirA_ki);        //motor maju
	PORT_motor |= (1<<dirB_ki);
		pwm(230,230);
//	pwm(220,220);
//	_delay_ms(300);
}








