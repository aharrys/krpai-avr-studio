#include <avr/io.h>
#include <util/delay.h>
#include "semua.h"


#define Right	7
#define FR		6
#define Front	5
#define	FL		4
#define	Left	3

void delay (uint8_t us)				// fungsi delay 
{
	TCNT0=255-us;
	TCCR0 |=(1<<CS01);	//nyalakan timer
	loop_until_bit_is_set (TIFR, TOV0);			// cek tov0?, jika set 
	TIFR |=(1<<TOV0);							// membuat tifr menjadi 0
	TCCR0 =0;					/// matikan timer
}



uint16_t get_distance(uint8_t posisi)   // fungsi uson
{
	// clear overflow
	uint8_t count_OVF=0;
	uint16_t distance; 

	DDRC |= (1<<posisi);
	PORTC |= (1<<posisi);
	delay (10);
	PORTC &=~(1<<posisi);
	DDRC &=~(1<<posisi);
	TCNT0 =0;
	loop_until_bit_is_set (PINC,posisi);
	TCCR0 |=(1<<CS01);
	
		while (bit_is_set(PINC,posisi)) 
		{//1221
			if (bit_is_set(TIFR,TOV0))			 
			{
				count_OVF++; 			 // menghitung over flow 
		 		TIFR |= (1<<TOV0);       // mengeset tov0 menjadi 0
			}
		}
		TCCR0 = 0;					// matikan timer
		distance = (count_OVF * 256 + TCNT0)/58;
		if (distance>=254)
		{
		distance=255;
		}
		for (i=0;i<10;i++)
		delay (5);
		return distance ;//(count_OVF * 256 + TCNT0)/58;
}

void lihat_lcd()
{
clrscr();
			goto_xy(1,1);printf("%3d %3d %3d %3d",get_distance(Right),get_distance(FR),get_distance(Front),get_distance(FL));
			goto_xy(2,1);printf("%3d",get_distance(Left));
			_delay_ms(50);

}

